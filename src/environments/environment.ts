// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

interface env {
  firebaseConfig: {
    [key: string]: any;
  };
  production: boolean;
  loadingTime: number;
  baseUrl: string;
}

export const environment: env = {
  firebaseConfig: {
    projectId: 'passwrd-web',
    appId: '1:372858537042:web:0f3d00970b53f6e2e48b8b',
    storageBucket: 'passwrd-web.appspot.com',
    locationId: 'europe-west',
    apiKey: 'AIzaSyA9Cy_xFlEjma-GRBPOoWOutZkriL-Mw3Q',
    authDomain: 'passwrd-web.firebaseapp.com',
    messagingSenderId: '372858537042',
  },
  production: false,
  loadingTime: 300,
  baseUrl: 'http://localhost:4200',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
import 'zone.js/plugins/zone-error'; // Included with Angular CLI.
