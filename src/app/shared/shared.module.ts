import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from './material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';

const SharedModules = [
  CommonModule,

  ReactiveFormsModule,
  FormsModule,

  MaterialModule,
];

@NgModule({
  imports: SharedModules,
  exports: [...SharedModules],
})
export class SharedModule {}
