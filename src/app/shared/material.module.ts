import { MatButtonModule } from '@angular/material/button';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { DividerModule } from 'primeng/divider';
import { PasswordModule } from 'primeng/password';
import { NgModule } from '@angular/core';
import { ButtonModule } from 'primeng/button';
import { MatIconModule } from '@angular/material/icon';
import { InputTextModule } from 'primeng/inputtext';
import { MenubarModule } from 'primeng/menubar';
import { MatCardModule } from '@angular/material/card';

const MatModules = [
  InputTextModule,
  MatButtonModule,
  ButtonModule,
  MatIconModule,
  PasswordModule,
  MenubarModule,
  DividerModule,
  ConfirmDialogModule,
  MatCardModule,
];

@NgModule({
  imports: MatModules,
  exports: MatModules,
})
export class MaterialModule {}
