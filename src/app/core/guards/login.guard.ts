import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  CanActivateChild,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Auth, user } from '@angular/fire/auth';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class LoginGuard implements CanActivate, CanActivateChild {
  constructor(private readonly router: Router, private readonly auth: Auth) {}
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot // | Observable<boolean | UrlTree> // | Promise<boolean | UrlTree> // | boolean // | UrlTree
  ): Observable<UrlTree | boolean> {
    user(this.auth).subscribe(console.log);
    return user(this.auth).pipe(
      map((v) => {
        const toReturn = !!!v;
        console.log(toReturn);
        if (!toReturn) {
          this.router.navigate(['/', 'app', 'dashboard']);
        }
        return toReturn;
      })
    );
  }

  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.canActivate(route, state);
  }
}
