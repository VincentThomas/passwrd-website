import { Injectable } from '@angular/core';
import {
  Router,
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot,
} from '@angular/router';
import { Observable, of, firstValueFrom } from 'rxjs';
import { Auth, user } from '@angular/fire/auth';

@Injectable({
  providedIn: 'root',
})
export class UserIdResolver implements Resolve<string> {
  constructor(private readonly auth: Auth) {}
  async resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Promise<string> {
    const usr = await firstValueFrom(user(this.auth));
    const { uid } = usr!;
    return uid;
  }
}
