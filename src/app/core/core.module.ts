import { AuthService } from './auth/auth.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@app/shared/shared.module';
import { StorageService } from './services/storage.service';
import { UserIdResolver } from './resolvers/user-id.resolver';
import { FirebaseModule } from './firebase.module';

@NgModule({
  declarations: [],
  providers: [AuthService, StorageService, UserIdResolver],
  imports: [CommonModule, SharedModule, FirebaseModule],
  exports: [FirebaseModule],
})
export class CoreModule {}
