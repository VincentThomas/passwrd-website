import { NgModule } from '@angular/core';
import { provideAuth, getAuth } from '@angular/fire/auth';
import { provideFirestore, getFirestore } from '@angular/fire/firestore';
// import { provideMessaging, getMessaging } from '@angular/fire/messaging';
// import { providePerformance, getPerformance } from '@angular/fire/performance';
import { initializeApp, provideFirebaseApp } from '@angular/fire/app';
import { environment } from 'src/environments/environment';

const ProvideFirebase: any = [
  provideFirebaseApp(() => initializeApp(environment.firebaseConfig)),
  provideAuth(() => getAuth()),
  provideFirestore(() => getFirestore()),
  // provideMessaging(() => getMessaging()),
  // providePerformance(() => getPerformance()),
];

@NgModule({
  imports: ProvideFirebase,
  exports: ProvideFirebase,
})
export class FirebaseModule {}
