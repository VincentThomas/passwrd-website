import { Injectable } from '@angular/core';
import {
  Auth,
  signOut,
  signInWithEmailAndPassword,
  signInWithPopup,
  UserCredential,
  AuthProvider,
  signInWithRedirect,
  user,
  createUserWithEmailAndPassword,
  User,
} from '@angular/fire/auth';
import { Observable, of } from 'rxjs';
@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(private readonly auth: Auth) {}

  getUser(): Observable<User | null> {
    return user(this.auth);
  }

  logOut() {
    signOut(this.auth)
      .then(() => window.location.reload())
      .catch((e) => console.log(e));
  }

  async createUser(
    email: string,
    password: string
  ): Promise<Observable<UserCredential>> {
    return of(await createUserWithEmailAndPassword(this.auth, email, password));
  }

  async emailSignIn(
    email: string,
    password: string
  ): Promise<Observable<UserCredential>> {
    return of(await signInWithEmailAndPassword(this.auth, email, password));
  }

  async redirectSignIn(provider: AuthProvider): Promise<never> {
    return await signInWithRedirect(this.auth, provider);
  }

  async popupSignIn(
    provider: AuthProvider
  ): Promise<Observable<UserCredential>> {
    return of(await signInWithPopup(this.auth, provider));
  }
}
