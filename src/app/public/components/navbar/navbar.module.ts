import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './navbar.component';
import { ButtonModule } from 'primeng/button';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { SidebarModule } from 'primeng/sidebar';

import { AvatarModule } from 'primeng/avatar';

@NgModule({
  declarations: [NavbarComponent],
  imports: [
    CommonModule,
    ButtonModule,
    MatIconModule,
    MatButtonModule,
    SidebarModule,
    AvatarModule,
  ],
  exports: [NavbarComponent],
})
export class NavbarModule {}
