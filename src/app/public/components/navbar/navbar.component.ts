import { Observable, map, firstValueFrom } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '@app/core/auth/auth.service';
import { User } from '@angular/fire/auth';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent implements OnInit {
  active = false;
  user$: Observable<User | null>;

  profilePic$: Observable<string | undefined | null>;

  constructor(
    private readonly auth: AuthService,
    private readonly router: Router
  ) {}

  ngOnInit(): void {
    this.user$ = this.auth.getUser();
    this.profilePic$ = this.user$.pipe(map((v) => v?.photoURL));
  }

  getProfilePic(): Observable<string> {
    return this.user$.pipe(
      map((user): string => {
        let toReturn = user?.photoURL;
        if (user?.photoURL === null) {
          toReturn = '';
        }
        return toReturn as string;
      })
    );
  }

  setActive() {
    this.active = !this.active;
  }

  toDashboard() {
    this.router.navigate(['app', 'dashboard']);
  }

  toLogin() {
    this.router.navigate(['auth', 'login']);
  }

  toNpm() {
    window.location.href = 'https://www.npmjs.com/package/passwrd';
  }

  toHome() {
    this.router.navigate(['/']);
    this.active = false;
  }

  toRegister() {
    this.router.navigate(['auth', 'register']);
  }
}
