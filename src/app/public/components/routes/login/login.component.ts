import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Auth } from '@angular/fire/auth';
import { environment } from 'src/environments/environment';
import {
  signInWithRedirect,
  GoogleAuthProvider,
  sendSignInLinkToEmail,
  GithubAuthProvider,
} from '@angular/fire/auth';
import { Firestore } from '@angular/fire/firestore';
import { MessageService } from 'primeng/api';

@Component({
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  errorMsg: string | null = null;
  form: FormGroup;

  constructor(
    private readonly formBuilder: FormBuilder,
    private readonly route: ActivatedRoute,
    private readonly auth: Auth,
    private readonly db: Firestore,
    private readonly msg: MessageService
  ) {}

  page: 'home' | 'email' = 'home';
  inputDisabled: boolean = true;

  async ngOnInit(): Promise<void> {
    // this.form = this.formBuilder.group({
    //   email: ['', [Validators.required, Validators.email]],
    // });

    // this.form.valueChanges.subscribe(() => {
    //   this.inputDisabled = !this.form.valid;
    // });
    this.googleAuth();
  }

  get emailErrors() {
    return this.form.get('email')?.errors;
  }

  async googleAuth() {
    const provider = new GoogleAuthProvider();
    await signInWithRedirect(this.auth, provider);
  }

  async githubAuth() {
    const provider = new GithubAuthProvider();
    provider.addScope('email');
    await signInWithRedirect(this.auth, provider);
  }

  gotoEmail() {
    this.page = 'email';
  }

  gotoHome() {
    this.page = 'home';
  }

  showPopup() {
    this.msg.add({
      key: 'tl',
      severity: 'success',
      summary: 'Email sent!',
      detail: 'An email has been sent to you with login details!',
    });
  }

  async emailAuth() {
    const email = this.form.get('email')?.value;
    const { baseUrl } = environment;
    await sendSignInLinkToEmail(this.auth, email, {
      handleCodeInApp: true,
      url: `${baseUrl}/auth/callback?email=${email}`,
    });
    this.showPopup();
    this.inputDisabled = true;
  }
}
