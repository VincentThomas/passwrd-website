import { Component, OnInit } from '@angular/core';
import { Auth, signInWithEmailLink } from '@angular/fire/auth';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-callback',
  templateUrl: './callback.component.html',
  styleUrls: ['./callback.component.scss'],
})
export class CallbackComponent implements OnInit {
  constructor(
    private readonly auth: Auth,
    private readonly route: ActivatedRoute,
    private readonly router: Router
  ) {}

  async ngOnInit(): Promise<void> {
    const email = this.route.snapshot.queryParamMap.get('email');
    try {
      await signInWithEmailLink(
        this.auth,
        email as string,
        window.location.href
      );
      this.router.navigate(['app', 'dashboard']);
    } catch (e) {
      this.router.navigate(['auth', 'login']);
    }
  }
}
