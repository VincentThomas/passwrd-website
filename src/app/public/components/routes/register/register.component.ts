import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '@core/auth/auth.service';
import { MessageService } from 'primeng/api';

@Component({
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {
  constructor(
    private readonly auth: AuthService,
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly fb: FormBuilder,
    private readonly msg: MessageService
  ) {}
  active: string | null;
  registerForm: FormGroup;
  errorMsg: string | null;

  goBack() {
    this.router.navigate(['..'], { relativeTo: this.route });
  }
  ngOnInit(): void {
    this.registerForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(7)]],
    });
    this.registerForm.controls?.['email'].valueChanges.subscribe(() => {
      this.errorMsg = null;
    });
  }
  async Register() {
    const { email, password } = this.registerForm.value;
    try {
      await this.auth.createUser(email, password);
      this.msg.add({
        severity: 'success',
        summary: 'User Added!',
        detail: 'User successfully added! Now its time to login!',
      });
    } catch (error: any) {
      if (error.code === 'auth/email-already-in-use') {
        this.errorMsg = 'Email is not available';
      }
    }
  }
}
