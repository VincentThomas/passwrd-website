import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './components/routes/login/login.component';
import { LoginGuard } from '@app/core/guards/login.guard';
import { RegisterComponent } from './components/routes/register/register.component';
import { HomeComponent } from './components/routes/home/home.component';
import { PublicComponent } from './public.component';
import { CallbackComponent } from './components/routes/callback/callback.component';

const routes: Routes = [
  {
    path: '',
    component: PublicComponent,
    children: [
      {
        path: '',
        component: HomeComponent,
        data: { title: 'Home' },
      },
    ],
  },
  {
    path: 'auth',
    children: [
      {
        path: 'login',
        component: LoginComponent,
        canActivate: [LoginGuard],
        data: { title: 'Login' },
      },
      {
        path: 'register',
        canActivate: [LoginGuard],
        component: RegisterComponent,
        data: { title: 'Register' },
      },
      {
        path: 'callback',
        component: CallbackComponent,
        data: { title: 'Loading...' },
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PublicRoutingModule {}
