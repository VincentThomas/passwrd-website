import { NgModule } from '@angular/core';

import { PublicRoutingModule } from './public-routing.module';
import { PublicComponent } from './public.component';
import { SharedModule } from '@app/shared/shared.module';
import { LoginComponent } from './components/routes/login/login.component';
import { RegisterComponent } from './components/routes/register/register.component';
import { HomeComponent } from './components/routes/home/home.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { MessageService } from 'primeng/api';
import { ToastModule } from 'primeng/toast';
import { CallbackComponent } from './components/routes/callback/callback.component';
import { NavbarModule } from './components/navbar/navbar.module';

@NgModule({
  declarations: [
    PublicComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    NotFoundComponent,
    CallbackComponent,
  ],
  imports: [SharedModule, PublicRoutingModule, ToastModule, NavbarModule],
  providers: [MessageService],
})
export class PublicModule {}
