import { SharedModule } from '@app/shared/shared.module';
import { MessageService } from 'primeng/api';
import { NavbarModule } from './components/navbar/navbar.module';
import { ToastModule } from 'primeng/toast';
import { PublicRoutingModule } from './public-routing.module';
import { CallbackComponent } from './components/routes/callback/callback.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { HomeComponent } from '@app/private/pages/dashboard/home/home.component';
import { RegisterComponent } from './components/routes/register/register.component';
import { LoginComponent } from './components/routes/login/login.component';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicComponent } from './public.component';

describe('PublicComponent', () => {
  let component: PublicComponent;
  let fixture: ComponentFixture<PublicComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        PublicComponent,
        LoginComponent,
        RegisterComponent,
        HomeComponent,
        NotFoundComponent,
        CallbackComponent,
      ],
      imports: [SharedModule, PublicRoutingModule, ToastModule, NavbarModule],
      providers: [MessageService],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
