import { Router } from '@angular/router';
import { Credential } from '@private/models/credential.model';
import { Observable, of } from 'rxjs';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'home-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  constructor(private readonly router: Router) {}

  @Input('websites') websites$: Observable<Credential[]> = of([]);
  @Input('notes') notes$: Observable<any>;
  @Input('contacts') contacts$: Observable<any>;

  ngOnInit() {
    this.websites$.subscribe(console.log);
  }

  toWebsite() {
    this.router.navigate(['/', 'app', 'dashboard', 'l']);
  }

  toNotes() {
    this.router.navigate(['/', 'app', 'dashboard', 'n']);
  }

  toContacts() {}
}
