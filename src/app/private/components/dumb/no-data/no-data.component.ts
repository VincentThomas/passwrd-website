import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-no-data',
  templateUrl: './no-data.component.html',
  styleUrls: ['./no-data.component.scss'],
})
export class NoDataComponent implements OnInit {
  constructor() {}

  @Input('what') public what: string;
  @Input('plural') public plural: string;

  ngOnInit(): void {}
}
