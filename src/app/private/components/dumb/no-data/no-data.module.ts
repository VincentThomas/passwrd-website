import { NoDataComponent } from '@app/private/components/dumb/no-data/no-data.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [CommonModule],
  declarations: [NoDataComponent],
  exports: [NoDataComponent],
})
export class NoDataModule {}
