// ? Core
import { NgModule } from '@angular/core';

import { PrivateComponent } from './private.component';

import { MenubarModule } from 'primeng/menubar';
import { PrivateRoutingModule } from './private-routing.module';

const UIModules = [MenubarModule];

@NgModule({
  declarations: [PrivateComponent],
  imports: [PrivateRoutingModule, ...UIModules],
})
export class PrivateModule {}
