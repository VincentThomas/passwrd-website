export interface Credential {
  website: string;
  username: string;
  password: string;
}
