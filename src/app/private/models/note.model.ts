export interface Note {
  title: string;
  tags: string[];
  note: string;
}
