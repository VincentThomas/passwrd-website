import { map, Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '@app/core/auth/auth.service';
import { Location } from '@angular/common';
import { MenuItem } from 'primeng/api';
import { User } from '@angular/fire/auth';

@Component({
  templateUrl: './private.component.html',
  styleUrls: ['./private.component.scss'],
})
export class PrivateComponent implements OnInit {
  constructor(
    private readonly router: Router,
    public readonly auth: AuthService,
    public readonly location: Location
  ) {}
  user$: Observable<User | null>;
  photoURL: any;
  websites$: any;
  open: boolean = false;
  items: MenuItem[] = [
    {
      label: 'Home',
      icon: 'pi pi-fw pi-home',
      routerLink: ['dashboard'],
    },
    {
      label: 'Websites',
      icon: 'pi pi-fw pi-table',
      routerLink: ['dashboard', 'l'],
    },
    {
      label: 'Notes',
      icon: 'pi pi-fw pi-file',

      routerLink: ['dashboard', 'n'],
    },
    {
      label: 'Profile',
      icon: 'pi pi-fw pi-user',
      items: [
        {
          label: 'Quit',
          icon: 'pi pi-fw bx bx-window-close',
          command: () => {
            this.router.navigate(['/']);
          },
        },
        {
          label: 'Log out',
          icon: 'pi pi-fw bx bx-log-out',
          command: () => {
            this.auth.logOut();
          },
        },
      ],
    },
  ];

  ngOnInit(): void {
    this.user$ = this.auth.getUser();
  }
}
