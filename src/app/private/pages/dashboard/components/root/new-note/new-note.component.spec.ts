import { ChipsModule } from 'primeng/chips';
import { ReactiveFormsModule } from '@angular/forms';
import { ButtonModule } from 'primeng/button';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { CommonModule } from '@angular/common';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { InputTextModule } from 'primeng/inputtext';
import { NewNoteComponent } from './new-note.component';
import { FormsModule } from '@angular/forms';
describe('NewNoteComponent', () => {
  let component: NewNoteComponent;
  let fixture: ComponentFixture<NewNoteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [NewNoteComponent],
      imports: [
        CommonModule,
        InputTextareaModule,
        InputTextModule,
        ButtonModule,
        ReactiveFormsModule,
        ChipsModule,
        FormsModule,
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewNoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
