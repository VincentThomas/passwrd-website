import { Component, OnInit, Input } from '@angular/core';
import { StorageService } from '@app/core/services/storage.service';
import { AuthService } from '@app/core/auth/auth.service';

@Component({
  selector: 'smart-note-card',
  templateUrl: './note-card.component.html',
  styleUrls: ['./note-card.component.scss'],
})
export class NoteCardComponent implements OnInit {
  constructor(
    private readonly storage: StorageService,
    private readonly auth: AuthService
  ) {}

  @Input('note') note: any;

  ngOnInit(): void {}

  removeNote(note: any) {
    this.auth.getUser().subscribe(({ uid }: any) => {
      this.storage.removeNote(uid, note);
    });
    console.log(note);
  }
}
