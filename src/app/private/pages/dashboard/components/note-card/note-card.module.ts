import { ChipModule } from 'primeng/chip';
import { NoteCardComponent } from './note-card.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';

@NgModule({
  declarations: [NoteCardComponent],
  imports: [CommonModule, ChipModule, MatButtonModule, MatIconModule],
  exports: [NoteCardComponent],
})
export class NoteCardModule {}
