import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { map } from 'rxjs';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-modify-data',
  templateUrl: './modify-data.component.html',
  styleUrls: ['./modify-data.component.scss'],
})
export class ModifyDataComponent implements OnInit {
  constructor(private readonly route: ActivatedRoute) {}

  ngOnInit(): void {}

  getTitle() {
    const length = this.route.children.length - 1;

    return this.route.children[length - 1]?.data.pipe(
      map((data: any) => data?.['title'])
    );

    // return this.route.children.at(-1)?.data.pipe(map((v: any) => v?.['title']));
  }
}
