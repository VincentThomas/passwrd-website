import { MessageService } from 'primeng/api';
import { NgModule, Input } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ModifyDataRoutingModule } from './modify-data-routing.module';
import { ModifyDataComponent } from './modify-data.component';
import { WebsiteComponent } from './website/website.component';
import { DividerModule } from 'primeng/divider';
import { InputTextModule } from 'primeng/inputtext';
import { ReactiveFormsModule } from '@angular/forms';
import { ButtonModule } from 'primeng/button';
import { ToastModule } from 'primeng/toast';
@NgModule({
  declarations: [ModifyDataComponent, WebsiteComponent],
  imports: [
    CommonModule,
    ModifyDataRoutingModule,
    DividerModule,
    ReactiveFormsModule,
    InputTextModule,
    ButtonModule,
    ToastModule,
  ],
  providers: [MessageService],
})
export class ModifyDataModule {}
