import { DialogService } from 'primeng/dynamicdialog';
import { StorageService } from '@core/services/storage.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '@app/core/auth/auth.service';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-website',
  templateUrl: './website.component.html',
  styleUrls: ['./website.component.scss'],
  providers: [MessageService],
})
export class WebsiteComponent implements OnInit {
  constructor(
    private readonly router: Router,
    private readonly fb: FormBuilder,
    private readonly auth: AuthService,
    private readonly storage: StorageService,
    private readonly msg: MessageService
  ) {}

  // display: boolean = false;

  form: FormGroup;
  websiteError: boolean;
  usernameError: boolean;
  passwordError: boolean;

  ngOnInit(): void {
    this.form = this.fb.group({
      website: ['', Validators.required],
      username: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  goBack() {
    this.router.navigate(['app', 'dashboard']);
  }

  addWebsite() {
    this.auth.getUser().subscribe((user) => {
      const uid = user?.uid as string;

      const { website, username, password } = this.form.value;

      this.storage.addWebsite(website, username, password, uid);
      this.router.navigate(['/', 'app', 'dashboard', 'l']).then(() => {
        this.msg.add({
          severity: 'success',
          summary: 'Created',
          detail: 'Website created successfully!',
        });
        console.log('yes');
      });
    });
  }
}
