import { WebsiteComponent } from './website/website.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ModifyDataComponent } from './modify-data.component';

const routes: Routes = [
  {
    path: '',
    component: ModifyDataComponent,
    children: [
      {
        path: 'w',
        component: WebsiteComponent,
        data: { title: 'Add Website' },
      },
    ],
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ModifyDataRoutingModule {}
