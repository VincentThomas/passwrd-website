import { NoteCardModule } from '@app/private/pages/dashboard/components/note-card/note-card.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotesComponent } from './notes.component';
import { InplaceModule } from 'primeng/inplace';
import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';
import { NoDataModule } from '@app/private/components/dumb/no-data/no-data.module';

@NgModule({
  declarations: [NotesComponent],
  imports: [
    CommonModule,
    InplaceModule,
    ButtonModule,
    InputTextModule,
    NoteCardModule,
    NoDataModule,
  ],
})
export class NotesModule {}
