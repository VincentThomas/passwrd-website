import { Observable, Subscription } from 'rxjs';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { StorageService } from '@app/core/services/storage.service';
import { AuthService } from '@app/core/auth/auth.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  templateUrl: './notes.component.html',
  styleUrls: ['./notes.component.scss'],
})
export class NotesComponent implements OnInit, OnDestroy {
  constructor(
    private readonly storage: StorageService,
    private readonly auth: AuthService,
    private readonly route: ActivatedRoute
  ) {}

  notes$: Observable<any>;
  sub: Subscription;

  ngOnInit(): void {
    this.sub = this.route.data.subscribe(async ({ userId }) => {
      this.notes$ = await this.storage.getNotes(userId);
    });
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }
}
