import { MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { SplitButtonModule } from 'primeng/splitbutton';
import { FooterComponent } from './../../components/dumb/footer/footer.component';
import { ButtonModule } from 'primeng/button';
import { DashboardComponent } from '@private/pages/dashboard/dashboard.component';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { HomeModule } from './home/home.module';
import { NotesModule } from './notes/notes.module';
import { WebsitesModule } from './websites/websites.module';
import { NoDataModule } from '@app/private/components/dumb/no-data/no-data.module';
import { NotesComponent } from '@app/private/pages/dashboard/notes/notes.component';
import { WebsitesComponent } from '@app/private/pages/dashboard/websites/websites.component';
import { UserIdResolver } from '@core/resolvers/user-id.resolver';
import { HomeComponent } from '@app/private/pages/dashboard/home/home.component';
import { RouterModule, Routes } from '@angular/router';
import { NewWebsiteModule } from './components/root/new-website/new-website.module';
import { NewNoteModule } from './components/root/new-note/new-note.module';
import { DividerModule } from 'primeng/divider';

const PageModules = [HomeModule, NotesModule, WebsitesModule];

const Components = [DashboardComponent, FooterComponent];

// Base: /app/dashboard
const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    children: [
      {
        path: '',
        component: HomeComponent,
        resolve: {
          userId: UserIdResolver,
        },
        data: { title: 'Overview' },
      },
      {
        path: 'l',
        component: WebsitesComponent,
        resolve: {
          userId: UserIdResolver,
        },
        data: { title: 'Websites' },
      },
      {
        path: 'n',
        component: NotesComponent,
        resolve: {
          userId: UserIdResolver,
        },
        data: { title: 'Notes' },
      },
    ],
  },
];

@NgModule({
  imports: [
    CommonModule,
    ...PageModules,
    // ? Layout,
    NoDataModule,
    ButtonModule,
    RouterModule.forChild(routes),
    NewWebsiteModule,
    SplitButtonModule,
    NewNoteModule,
    DividerModule,
  ],
  providers: [DialogService, MessageService],
  declarations: [...Components],
  exports: [RouterModule],
})
export class DashboardModule {}
