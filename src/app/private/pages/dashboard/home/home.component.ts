import { Credential } from '@private/models/credential.model';
import { ActivatedRoute } from '@angular/router';
import { Observable, of } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import {
  Firestore,
  collection as col,
  collectionData,
  doc,
  setDoc,
} from '@angular/fire/firestore';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  constructor(
    private readonly db: Firestore,
    private readonly route: ActivatedRoute
  ) {}
  websites$: Observable<Credential[]>;
  notes$: Observable<any>;
  contacts$: Observable<any>;

  ngOnInit(): void {
    const state$ = this.route.data;
    state$.subscribe((state) => {
      const { userId: uid } = state;
      this.websites$ = this.getWebsites(uid);
      this.notes$ = this.getNotes(uid);
    });
  }

  getWebsites(uid: string) {
    const colRef = col(this.db, `storage/${uid}/websites`);
    return collectionData(colRef, {
      idField: 'website',
    }) as Observable<Credential[]>;
  }

  getContacts(uid: string) {
    const colRef = col(this.db, `storage/${uid}/contacts`);
    return collectionData(colRef, { idField: 'author' });
  }

  getNotes(uid: string) {
    const colRef = col(this.db, `storage/${uid}/notes`);
    return collectionData(colRef, {
      idField: 'name',
    });
  }
}
