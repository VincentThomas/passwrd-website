import { CommonModule } from '@angular/common';
import { HeaderModule } from '../../../components/home/header/header.module';
import { NgModule } from '@angular/core';
import { HomeComponent } from './home.component';
import { DividerModule } from 'primeng/divider';

@NgModule({
  declarations: [HomeComponent],
  imports: [DividerModule, HeaderModule, CommonModule],
})
export class HomeModule {}
