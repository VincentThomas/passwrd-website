import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable, firstValueFrom } from 'rxjs';
import { Auth, signOut, user } from '@angular/fire/auth';
import {
  collection,
  Firestore,
  collectionData,
  doc,
  deleteDoc,
  getDoc,
} from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { AuthService } from '@app/core/auth/auth.service';
import { StorageService } from '@app/core/services/storage.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  templateUrl: 'websites.component.html',
  styleUrls: ['websites.component.scss'],
})
export class WebsitesComponent implements OnInit, OnDestroy {
  constructor(
    private readonly router: Router,
    private readonly auth: AuthService,
    private readonly db: Firestore,
    private readonly storage: StorageService,
    private readonly route: ActivatedRoute
  ) {}

  websites$: Observable<any>;
  async ngOnInit(): Promise<void> {
    const user = await firstValueFrom(this.auth.getUser());
    this.websites$ = this.storage.getWebsites(user?.uid as string);
  }
  ngOnDestroy(): void {}

  gotoAddWebsite() {
    this.router.navigate(['app', 'modify', 'w']);
  }

  async logOut() {
    this.auth.logOut();
  }

  async removeWebsite(website: string) {
    // const usr = await firstValueFrom(this.auth.getUser());
    this.route.data.subscribe(async ({ userId }) => {
      const ref = doc(this.db, `storage/${userId}/websites/${website}`);
      await deleteDoc(ref);
    });
  }
}
