import { NgModule } from '@angular/core';
import { WebsitesComponent } from './websites.component';
import { NoDataModule } from '@app/private/components/dumb/no-data/no-data.module';
import { CommonModule } from '@angular/common';
import { WebsiteCardComponent } from '@app/private/pages/dashboard/components/websites/card/website-card.component';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { ClipboardModule } from '@angular/cdk/clipboard';

@NgModule({
  declarations: [WebsitesComponent, WebsiteCardComponent],
  imports: [
    NoDataModule,
    CommonModule,
    MatCardModule,
    MatButtonModule,
    MatMenuModule,
    MatIconModule,
    ClipboardModule,
  ],
})
export class WebsitesModule {}
