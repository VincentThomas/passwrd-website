import { DialogService } from 'primeng/dynamicdialog';
import { map } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { Component } from '@angular/core';
import { NewWebsiteComponent } from './components/root/new-website/new-website.component';
import { NewNoteComponent } from './components/root/new-note/new-note.component';
import { MessageService } from 'primeng/api';

@Component({
  templateUrl: 'dashboard.component.html',
  styleUrls: ['dashboard.component.scss'],
})
export class DashboardComponent {
  constructor(
    private readonly route: ActivatedRoute,
    public readonly dialog: DialogService,
    private readonly msg: MessageService
  ) {}
  items = [
    { label: 'Website', icon: 'pi pi-link', command: () => this.newWebsite() },
    { label: 'Secure note', icon: 'pi pi-file', command: () => this.newNote() },
    { label: 'Contact', icon: 'pi pi-user', command: () => this.newContact() },
  ];

  getTitle() {
    const length = this.route.children.length - 1;

    return this.route.children[length - 1]?.data.pipe(
      map((v: any) => v?.['title'])
    );
  }

  new(comp: any, title: string) {
    const ref = this.dialog.open(comp, {
      closable: true,
      closeOnEscape: true,
      header: `Add ${title}`,
      width: '90%',
      style: { 'max-width': '40rem' },
    });
    ref.onClose.subscribe((obj) => {
      if (obj) {
        this.msg.add({
          severity: obj.type,
          summary: obj.title,
          detail: obj.desc,
        });
      }
    });
  }

  newWebsite() {
    const comp = NewWebsiteComponent;
    this.new(comp, 'Credential');
  }

  newNote() {
    const comp = NewNoteComponent;
    this.new(comp, 'Secure Note');
  }

  newContact() {}
}
