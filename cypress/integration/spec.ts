describe('Home Page', () => {
  it('Visits the home page', () => {
    cy.visit('/');
    cy.get('nav h1').contains('Passwrd');
    cy.contains('sandbox app is running!');
  });
});
